import Vue from 'vue'
import Vuex from 'vuex'
import createPersist from 'vuex-localstorage'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        finished_loading: false,
        cards_loaded: 0
    },
    mutations: {
        FINISH_LOADING (state) {
            state.cards_loaded++;
            if(state.cards_loaded <= 3) {
                state.finished_loading = true;
            }
        },
        FIRE_INIT_RERENDER (state) {
            state.initial_rerender_fired = true;
        },
        UPDATE_SETTING (state, args) {
            state.settings[args.setting] = args.state;
        },
        UPDATE_DATA (state, args) {
            state.data[args.data] = args.state;
        }
    },
    actions: {
        finish_loading ({commit}) {
            commit('FINISH_LOADING');
        },
        fire_init_rerender ({commit}) {
            commit('FIRE_INIT_RERENDER');
        },
        update_setting ({commit}, args) {
            commit('UPDATE_SETTING', args);
        },
        update_data ({commit}, args) {
            commit('UPDATE_DATA', args);
        }
    },
    plugins: [createPersist({
        namespace: 'root',
        initialState: {
            settings: {
                theme:{
                    inner: {
                        primary: 'green',
                        accent: 'green',
                        warn: 'red',
                        background: 'white'
                    },
                    body_color_class: 'white-bg'
                },
                show_computer_info: true,
                show_power_info: true,
                show_bookmark_bar: true,
                show_other_bookmarks: true,
                show_mobile_bookmarks: true,
                use_alt_bookmark_folders: false,
                use_24_hour_clock: true,
                show_seconds: true,
                center_clock: true,
                use_imperial_date_format: false,
                show_top_sites: true,
                show_downloads: true
            },
            data: {
                note: null
            }
        },
        expires: 0
    })]
})
