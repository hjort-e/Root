import Vue from 'vue'
import VueMaterial from 'vue-material';
import App from './App.vue'
import store from './store'

Vue.use(VueMaterial);

function getDateTime(){
    const dateTime = new Date();
    let hours = dateTime.getHours();
    let minutes = dateTime.getMinutes();
    let seconds = dateTime.getSeconds();

    if(hours < 10) {hours = '0' + hours;}
    if(minutes < 10) {minutes = '0' + minutes;}
    if(seconds < 10) {seconds = '0' + seconds;}

    return `${hours}:${minutes}:${seconds}`;
}

document.title = `NewTab - ${getDateTime()}`;
setInterval(function() {
    document.title = `NewTab - ${getDateTime()}`;
}, 1000); // milliseconds

Vue.config.productionTip = false

new Vue({
  store,
  render: h => h(App)
}).$mount('#app')